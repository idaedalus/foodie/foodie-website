import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Spin } from 'antd';
import axios from '../../axios/nodeInstance';
import { get } from '../../util/localStorage';
import Post from '../../components/Post/Post';

import { Card, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import classes from './FeedTest.module.css';

class Feed extends React.Component {
    state = {
        data: [],
        hasMoreItems: true,
        page: 0,
        limit: 10,
        loading: false,
    };

    fetchData = page => {
        if (this.state.loading) {
            return;
        }
        console.log('getting pagae', this.state.page);
        this.setState({
            loading: true,
            hasMoreItems: false,
        });
        axios
            .get('/posts', {
                params: {
                    username: get('username'),
                    page: this.state.page,
                    limit: this.state.limit,
                },
            })
            .then(response => {
                let posts = response.data;
                console.log('setting state');

                this.setState(prevState => {
                    return {
                        data: prevState.data.concat(posts),
                        page: prevState.page + 1,
                        loading: false,
                        hasMoreItems: true,
                    };
                });
                console.log('set state');
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    loading: false,
                });
            });
    };

    render() {
        const loader = this.state.loading ? <Spin /> : null;

        let items = [];
        this.state.data.map((content, index) => {
            items.push(
                <Post
                    key={content._id}
                    postID={content._id}
                    likes={content.likes}
                    liked_by={content.liked_by}
                    imageUrl={content.contentUrl}
                    username={content.user.username}
                    picture={content.user.picture}
                    title={content.description.caption}
                    date={content.description.createdAt}
                    description={content.description.content}
                    restaurant={content.description.restaurant}
                    recipe={content.description.recipe}
                    userID={this.props.userID}
                />
            );
        });

        return (
            <div
                className={classes.Container}
                ref={ref => (this.scrollParentRef = ref)}>
                <Card fluid>
                    <Card.Content>
                        <Card.Header>Feed</Card.Header>
                        <Card.Description>
                            Your personal Qripsy frontpage. Come here to see
                            latest trends about food and much more
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <Link to="/submit">
                            <Button primary fluid>
                                Create Post
                            </Button>
                        </Link>
                    </Card.Content>
                </Card>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.fetchData}
                    hasMore={this.state.hasMoreItems}
                    loader={loader}
                    threshold={500}
                    useWindow={false}
                    getScrollParent={() => this.scrollParentRef}>
                    {items}
                </InfiniteScroll>
                <div style={{ textAlign: 'center' }}>{loader}</div>
            </div>
        );
    }
}

export default Feed;
