import React from 'react';
import { Card, Button, Form as SemanticForm, Message } from 'semantic-ui-react';
import { withFormik, Form } from 'formik';
import Input from '../../../components/UI/Form/Input/Input';
import * as Yup from 'yup';
import axiosInstance from '../../../axios/withoutHeaders';
import { set, get } from '../../../util/localStorage';
import classes from './LoginCard.module.css';

const LoginCard = ({ values, errors, touched, isSubmitting }) => {
    return (
        <Card className={classes.LoginCard}>
            <Card.Content>
                <Card.Header>
                    <h1>Login</h1>
                </Card.Header>

                <Form className={classes.LoginForm}>
                    <Input
                        label="Email"
                        name="email"
                        placeholder="Email"
                        type="email"
                        errors={errors.email}
                        touched={touched.email}
                    />
                    <Input
                        label="Password"
                        name="password"
                        placeholder="Password"
                        type="password"
                        errors={errors.password}
                        touched={touched.password}
                    />

                    <Button primary disabled={isSubmitting}>
                        Login
                    </Button>
                </Form>
            </Card.Content>
        </Card>
    );
};

const FormikForm = withFormik({
    mapPropsToValues({ email, password }) {
        return {
            email: email || '',
            password: password || '',
        };
    },
    validationSchema: Yup.object().shape({
        email: Yup.string()
            .email('Email is not valid')
            .required('Email is required'),
        password: Yup.string()
            .min(6, 'Password must be 6 characters or longer')
            .required('Password is required'),
    }),
    handleSubmit(values, { resetForm, setErrors, setSubmitting }) {
        axiosInstance
            .post('/auth/signin/', {
                email: values.email,
                password: values.password,
            })
            .then(res => {
                if (res.data.token) {
                    set('authToken', res.data.token);
                    set('access_token', res.data.accessToken);
                    set('email', values.email);
                    set('username', res.data.username);
                    resetForm();
                    setSubmitting(false);
                    window.location.replace('/');
                }
            })
            .catch(err => {
                setErrors({ email: err });
                setSubmitting(false);
            });
    },
})(LoginCard);

export default FormikForm;
