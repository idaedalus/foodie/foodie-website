import React, { Component } from 'react';
import LoginCard from './LoginCard/LoginCard';
import RegisterCard from './RegisterCard/RegisterCard';
import { Grid } from 'semantic-ui-react';

class Auth extends Component {
    render() {
        return (
            <React.Fragment>
                <Grid stackable columns={3}>
                    <Grid.Column floated="left" width={7}>
                        <LoginCard />
                    </Grid.Column>
                    <Grid.Column verticalAlign='middle' textAlign='center' width={2}>
                        <div>OR</div>
                    </Grid.Column>
                    <Grid.Column floated="right" width={7}>
                        <RegisterCard />
                    </Grid.Column>
                </Grid>

                {/* <Switch>
                    <Route path="/auth/login" component={LoginCard} />
                    <Route path="/auth/register" component={RegisterCard} />
                    <Route path="/auth" component={LoginCard} />
                </Switch> */}
            </React.Fragment>
        );
    }
}

export default Auth;
