import React from 'react';
import { Card, Button } from 'semantic-ui-react';
import { Form, Formik } from 'formik';
import Input from '../../../components/UI/Form/Input/Input';
import Select from '../../../components/UI/Form/Select/Select';
import DatePicker from '../../../components/UI/Form/DatePicker/DatePicker';
import * as Yup from 'yup';
import axiosInstance from '../../../axios/withoutHeaders';
import classes from './RegisterCard.module.css';

const genders = [
    { key: 'm', label: 'Male', value: 'Male' },
    { key: 'f', label: 'Female', value: 'Female' },
    { key: 'o', label: 'Other', value: 'Other' },
];

const RegisterCard = ({ values, errors, touched, isSubmitting }) => {
    return (
        <Formik
            initialValues={{
                firstName: '',
                lastName: '',
                gender: '',
                dob: '22/06/2019',
                username: '',
                password: '',
                email: '',
            }}
            onSubmit={(values, { setSubmitting, setErrors, resetForm }) => {
                axiosInstance
                    .post('/auth/signup/', {
                        emailOrMobile: values.email,
                        password: values.password,
                        firstName: values.firstName,
                        lastName: values.lastName,
                        username: values.username,
                        dob: values.dob,
                        gender: values.gender.value,
                    })
                    .then(res => {
                        console.log(res.data.message);
                        if (res.data.message === 'User already exists') {
                            setErrors({ email: 'User already exists' });
                        }

                        if (res.data.message === 'Username is taken') {
                            setErrors({ username: 'User already exists' });
                        }

                        if (res.data.message === 'User created') {
                            resetForm();
                            setSubmitting(false);
                            window.location.replace('/');
                        }
                    })
                    .catch(err => {
                        setErrors({ email: err.data });
                        setSubmitting(false);
                    });
            }}
            validationSchema={Yup.object().shape({
                email: Yup.string()
                    .email('Email is not valid')
                    .required('Email is required'),
                password: Yup.string()
                    .min(6, 'Password must be 6 characters or longer')
                    .required('Password is required'),
                firstName: Yup.string()
                    .min(3, 'First name must be 3 characters or longer')
                    .required('First Name is a required field'),
                lastName: Yup.string()
                    .min(3, 'Last name must be 3 characters or longer')
                    .required('Last Name is a required field'),
                username: Yup.string()
                    .min(3, 'Username must be 3 characters or longer')
                    .required('Username is a required field'),
                gender: Yup.string().required('Gender is a required field'),
                dob: Yup.string().required('DOB is a required field'),
            })}>
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    setFieldValue,
                } = props;
                return (
                    <Card className={classes.RegisterCard}>
                        <Card.Content>
                            <Card.Header>
                                <h1>Register</h1>
                            </Card.Header>

                            <Form className={classes.Form}>
                                <Input
                                    label="First name"
                                    name="firstName"
                                    placeholder="First name"
                                    type="text"
                                    errors={errors.firstName}
                                    touched={touched.firstName}
                                />
                                <Input
                                    label="Last name"
                                    name="lastName"
                                    placeholder="Last name"
                                    type="text"
                                    errors={errors.lastName}
                                    touched={touched.lastName}
                                />

                                <Input
                                    label="Username"
                                    name="username"
                                    placeholder="Username"
                                    type="text"
                                    errors={errors.username}
                                    touched={touched.username}
                                />

                                <DatePicker
                                    label="DOB"
                                    name="dob"
                                    placeholder="DD/MM/YY"
                                    type="text"
                                    errors={errors.dob}
                                    touched={touched.dob}
                                    value={values.dob}
                                    onChange={setFieldValue}
                                />

                                <Select
                                    label="Gender"
                                    name="gender"
                                    placeholder="Gender"
                                    errors={errors.gender}
                                    touched={touched.gender}
                                    onChange={setFieldValue}
                                    value={values.gender}
                                    options={genders}
                                />

                                <Input
                                    label="Email"
                                    name="email"
                                    placeholder="Email"
                                    type="email"
                                    errors={errors.email}
                                    touched={touched.email}
                                />
                                <Input
                                    label="Password"
                                    name="password"
                                    placeholder="Password"
                                    type="password"
                                    errors={errors.password}
                                    touched={touched.password}
                                />

                                <Button primary disabled={isSubmitting}>
                                    Register
                                </Button>
                            </Form>
                        </Card.Content>
                    </Card>
                );
            }}
        </Formik>
    );
};

export default RegisterCard;
