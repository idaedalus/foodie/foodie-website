import React from 'react';
import { Button, Grid } from 'semantic-ui-react';
import { Formik, Form } from 'formik';
import Input from '../../../../components/UI/Form/Input/Input';
import TextArea from '../../../../components/UI/Form/TextArea/TextArea';
import Switch from '../../../../components/UI/Form/Switch/Switch';
import Select from '../../../../components/UI/Form/Select/Select';
import Rate from '../../../../components/UI/Form/Rate/Rate';
import * as Yup from 'yup';
import axios from '../../../../axios/nodeInstance';
import PictureWall from '../../../../components/PictureWall/PictureWall';

const cost = [
    { key: 'l', label: 'Less', value: 'Less' },
    { key: 'm', label: 'Moderate', value: 'Moderate' },
    { key: 'e', label: 'Expensive', value: 'Expensive' },
];

const typeOfRecipe = [
    { key: 'v', label: 'Vegetarian', value: 'Vegetarian' },
    { key: 'n', label: 'Non-Vegetarian', value: 'Non-Vegetarian' },
    { key: 'e', label: 'Contains Egg', value: 'Contains Egg' },
];

const typeOfRestaurant = [
    { key: 'v', label: 'Vegetarian', value: 'Vegetarian' },
    { key: 'n', label: 'Non-Vegetarian', value: 'Non-Vegetarian' },
    { key: 'b', label: 'Both', value: 'Both' },
];

const Restaurant = props => {
    console.log(props.userId);

    return (
        <Formik
            initialValues={{
                title: '',
                caption: '',
                content: '',
                price: '',
                restaurantName: '',
                location: '',
                isBar: false,
                isValet: false,
                rating: 4,
                rating_desc: '',
                bookingRequired: false,
                typeOfDish: '',
                typeOfRestaurant: '',
                files: [],
            }}
            onSubmit={(values, { setSubmitting, setErrors, resetForm }) => {
                const config = {
                    headers: {
                        'Content-type': 'multipart/form-data',
                        accept: 'application/json',
                        'Accept-Language': 'en-US,en;q=0.8',
                    },
                };

                let formData = new FormData();
                formData.append('user', props.userId);
                formData.append('caption', values.caption);
                formData.append('content', values.content);
                formData.append('price', values.price);
                formData.append('restaurantName', values.restaurantName);
                formData.append('location', values.location);
                formData.append('isBar', values.isBar);
                formData.append('isValet', values.isValet);
                formData.append('rating', values.rating);
                formData.append('rating_desc', values.rating_desc);
                formData.append('bookingRequired', values.bookingRequired);
                formData.append('typeOfDish', values.typeOfDish.value);
                formData.append(
                    'typeOfRestaurant',
                    values.typeOfRestaurant.value
                );

                for (const file of values.files) {
                    formData.append('media', file);
                }
                axios
                    .post(
                        `/post?userID=${props.userId}&post_type=review`,
                        formData,
                        config
                    )
                    .then(function(response) {
                        console.log(response);
                        setSubmitting(false);
                        resetForm();
                    })
                    .catch(function(error) {
                        console.log(error.response);
                        setSubmitting(false);
                    });
            }}
            validationSchema={Yup.object().shape({
                caption: Yup.string()
                    .max(140)
                    .required('Enter a caption'),
                content: Yup.string()
                    .max(2200)
                    .required('Enter the content'),
                restaurantName: Yup.string().required(
                    'Enter the name of the restaurant'
                ),
                location: Yup.string().required('Enter location of restaurant'),
                typeOfDish: Yup.string().required('Select an option'),
                typeOfRestaurant: Yup.string().required('Select an option'),
                price: Yup.string().required('Select an option'),
                files: Yup.array().required('Select files to upload'),
                isBar: Yup.string().required('Select an option'),
                isValet: Yup.string().required('Select an option'),
                bookingRequired: Yup.string().required('Select an option'),
                rating: Yup.number().required('Please give a rating'),
                rating_desc: Yup.string().required('Write a description'),
            })}>
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    setFieldValue,
                } = props;
                console.log(values);

                return (
                    <Form>
                        <PictureWall
                            onChange={setFieldValue}
                            files={values.files}
                        />
                        {console.log(values.files)}

                        <Input
                            label="Name of restaurant"
                            name="restaurantName"
                            placeholder="Name of restaurant"
                            type="text"
                            errors={errors.restaurantName}
                            touched={touched.restaurantName}
                        />

                        <Input
                            label="Location"
                            name="location"
                            placeholder="Location"
                            type="text"
                            errors={errors.location}
                            touched={touched.location}
                        />

                        <Rate
                            label="Rating"
                            name="rating"
                            icon="star"
                            errors={errors.rating}
                            touched={touched.rating}
                            onChange={setFieldValue}
                            value={values.rating}
                        />

                        <Input
                            label="Description"
                            name="rating_desc"
                            placeholder="Write a few words about your rating"
                            type="text"
                            errors={errors.rating_desc}
                            touched={touched.rating_desc}
                        />

                        <Input
                            label="Title"
                            name="caption"
                            placeholder="Title"
                            type="text"
                            errors={errors.caption}
                            touched={touched.caption}
                        />

                        <Select
                            label="Price"
                            name="price"
                            placeholder="Cost for two people"
                            errors={errors.price}
                            touched={touched.price}
                            onChange={setFieldValue}
                            value={values.price}
                            options={cost}
                        />

                        <Switch
                            label="Is valet service available?"
                            name="isValet"
                            errors={errors.isValet}
                            touched={touched.isValet}
                            onChange={setFieldValue}
                            value={values.isValet}
                        />

                        <Switch
                            label="Does this place have a bar?"
                            name="isBar"
                            errors={errors.isBar}
                            touched={touched.isBar}
                            onChange={setFieldValue}
                            value={values.isBar}
                        />

                        <Switch
                            label="Is a reservation required?"
                            name="bookingRequired"
                            errors={errors.bookingRequired}
                            touched={touched.bookingRequired}
                            onChange={setFieldValue}
                            value={values.bookingRequired}
                        />

                        <Grid stackable columns="equal">
                            <Grid.Column>
                                <Select
                                    label="Type of Dish"
                                    name="typeOfDish"
                                    placeholder="Type of Dish"
                                    errors={errors.typeOfDish}
                                    touched={touched.typeOfDish}
                                    onChange={setFieldValue}
                                    value={values.typeOfDish}
                                    options={typeOfRecipe}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <Select
                                    label="Type of Restaurant"
                                    name="typeOfRestaurant"
                                    placeholder="Type of Restaurant"
                                    errors={errors.typeOfRestaurant}
                                    touched={touched.typeOfRestaurant}
                                    onChange={setFieldValue}
                                    value={values.typeOfRestaurant}
                                    options={typeOfRestaurant}
                                />
                            </Grid.Column>
                        </Grid>

                        <TextArea
                            label="Content"
                            name="content"
                            placeholder="Content"
                            type="text"
                            errors={errors.content}
                            touched={touched.content}
                            onChange={handleChange}
                            value={values.content}
                        />

                        <Button disabled={isSubmitting} primary type="submit">
                            Submit
                        </Button>
                    </Form>
                );
            }}
        </Formik>
    );
};

export default Restaurant;
