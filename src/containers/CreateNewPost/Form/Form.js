import React from 'react';
import { Card, Tab } from 'semantic-ui-react';
import Recipe from './Recipe/Recipe';
import Review from './Restaurant/Restaurant';

const Form = props => {
    const panes = [
        {
            menuItem: 'Recipe',
            render: () => (
                <Tab.Pane>
                    <Recipe userId={props.userId} />
                </Tab.Pane>
            ),
        },
        {
            menuItem: 'Review',
            render: () => (
                <Tab.Pane>
                    <Review userId={props.userId} />
                </Tab.Pane>
            ),
        },
    ];
    return (
        <>
            <Card fluid>
                <Card.Content>
                    <Card.Header>Create a post</Card.Header>
                </Card.Content>
            </Card>

            <Tab panes={panes} />
        </>
    );
};

export default Form;
