import React from 'react';
import { Button, Grid } from 'semantic-ui-react';
import { Formik, Form } from 'formik';
import Input from '../../../../components/UI/Form/Input/Input';
import TextArea from '../../../../components/UI/Form/TextArea/TextArea';
import Select from '../../../../components/UI/Form/Select/Select';
import * as Yup from 'yup';
import axios from '../../../../axios/nodeInstance';
import PictureWall from '../../../../components/PictureWall/PictureWall';

const typeOfFood = [
    { key: 'b', label: 'Breakfast', value: 'Breakfast' },
    { key: 'l', label: 'Lunch', value: 'Lunch' },
    { key: 'd', label: 'Dinner', value: 'Dinner' },
    { key: 's', label: 'Snacks', value: 'Snacks' },
    { key: 'be', label: 'Beverage', value: 'Beverage' },
    { key: 'a', label: 'Any Time', value: 'Any Time' },
];

const difficulty = [
    { key: 'l', label: 'Least', value: 'Least' },
    { key: 'm', label: 'Moderate', value: 'Moderate' },
    { key: 'd', label: 'Very Difficult', value: 'Very Difficult' },
];

const typeOfRecipe = [
    { key: 'v', label: 'Vegetarian', value: 'Vegetarian' },
    { key: 'n', label: 'Non-Vegetarian', value: 'Non-Vegetarian' },
    { key: 'e', label: 'Contains Egg', value: 'Contains Egg' },
];

const Recipe = props => {
    console.log(props.userId);

    return (
        <Formik
            initialValues={{
                title: '',
                caption: '',
                content: '',
                ingredients: '',
                cookingTime: '',
                typeOfFood: '',
                difficulty: '',
                typeOfRecipe: '',
                files: [],
            }}
            onSubmit={(values, { setSubmitting, setErrors, resetForm }) => {
                const config = {
                    headers: {
                        'Content-type': 'multipart/form-data',
                        accept: 'application/json',
                        'Accept-Language': 'en-US,en;q=0.8',
                    },
                };

                let formData = new FormData();
                formData.append('user', props.userId);
                formData.append('caption', values.caption);
                formData.append('content', values.content);
                formData.append('typeOfFood', values.typeOfFood.value);
                formData.append('typeOfRecipe', values.typeOfRecipe.value);
                formData.append('cooking_time', values.cookingTime);
                formData.append('difficulty', values.difficulty.value);
                formData.append('ingredients', values.ingredients);
                for (const file of values.files) {
                    formData.append('media', file);
                }
                console.log(values);

                axios
                    .post(
                        `/post?userID=${props.userId}&post_type=recipe`,
                        formData,
                        config
                    )
                    .then(function(response) {
                        console.log(response);
                        setSubmitting(false);
                        resetForm();
                    })
                    .catch(function(error) {
                        console.log(error.response);
                        setSubmitting(false);
                    });
            }}
            validationSchema={Yup.object().shape({
                caption: Yup.string()
                    .max(140)
                    .required('Enter a caption'),
                content: Yup.string()
                    .max(2200)
                    .required('Enter the content'),
                ingredients: Yup.string().required('Enter recipe ingredients'),
                cookingTime: Yup.string().required('Enter cooking time'),
                typeOfFood: Yup.string().required('Select an option'),
                difficulty: Yup.string().required('Select an option'),
                typeOfRecipe: Yup.string().required('Select an option'),
                files: Yup.array().required('Select files to upload'),
            })}>
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    setFieldValue,
                } = props;
                return (
                    <Form>
                        <PictureWall
                            onChange={setFieldValue}
                            files={values.files}
                        />

                        {console.log(values.files)}

                        <Input
                            label="Title"
                            name="caption"
                            placeholder="Title"
                            type="text"
                            errors={errors.caption}
                            touched={touched.caption}
                        />

                        <Grid stackable columns="equal">
                            <Grid.Column>
                                <Select
                                    label="Type of food"
                                    name="typeOfFood"
                                    placeholder="Type of food"
                                    errors={errors.typeOfFood}
                                    touched={touched.typeOfFood}
                                    onChange={setFieldValue}
                                    value={values.typeOfFood}
                                    options={typeOfFood}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <Select
                                    label="Difficulty"
                                    name="difficulty"
                                    placeholder="Difficulty"
                                    errors={errors.difficulty}
                                    touched={touched.difficulty}
                                    onChange={setFieldValue}
                                    value={values.difficulty}
                                    options={difficulty}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <Select
                                    label="Type of recipe"
                                    name="typeOfRecipe"
                                    placeholder="Type of recipe"
                                    errors={errors.typeOfRecipe}
                                    touched={touched.typeOfRecipe}
                                    onChange={setFieldValue}
                                    value={values.typeOfRecipe}
                                    options={typeOfRecipe}
                                />
                            </Grid.Column>
                        </Grid>

                        <Input
                            label="Ingredients"
                            name="ingredients"
                            placeholder="Ingredients"
                            type="text"
                            errors={errors.ingredients}
                            touched={touched.ingredients}
                        />

                        <Input
                            label="Cooking time"
                            name="cookingTime"
                            placeholder="In Minutes"
                            type="text"
                            errors={errors.cookingTime}
                            touched={touched.cookingTime}
                        />

                        <TextArea
                            label="Content"
                            name="content"
                            placeholder="Content"
                            type="text"
                            errors={errors.content}
                            touched={touched.content}
                            onChange={handleChange}
                            value={values.content}
                        />

                        <Button disabled={isSubmitting} primary type="submit">
                            Submit
                        </Button>
                    </Form>
                );
            }}
        </Formik>
    );
};

export default Recipe;
