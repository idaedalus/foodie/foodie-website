import React from 'react';
import { Grid } from 'semantic-ui-react';
import Guidelines from './Guidelines/Guidelines';
import Form from './Form/Form';
import classes from './CreateNewPost.module.css';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

const CreateNewPost = props => {
    return (
        <Grid padded columns="equal">
            <Grid.Column>
                <Form userId={props.profile.userID} />
            </Grid.Column>
            <Grid.Column only="computer" width={4} floated="right">
                <Guidelines />
            </Grid.Column>
        </Grid>
    );
};

const mapStateToProps = state => {
    return {
        profile: state.profile,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        storeData: userData => dispatch(actionCreators.store(userData)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateNewPost);
