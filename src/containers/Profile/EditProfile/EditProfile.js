import React from 'react';
import { Card, Button, Grid } from 'semantic-ui-react';
import { Upload } from 'antd';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import Input from '../../../components/UI/Form/Input/Input';
import Select from '../../../components/UI/Form/Select/Select';
import TextArea from '../../../components/UI/Form/TextArea/TextArea';
import DatePicker from '../../../components/UI/Form/DatePicker/DatePicker';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import { Link } from 'react-router-dom';
import classes from './EditProfile.module.css';
import axios from '../../../axios/nodeInstance';

const genders = [
    { key: 'm', label: 'Male', value: 'Male' },
    { key: 'f', label: 'Female', value: 'Female' },
    { key: 'o', label: 'Other', value: 'Other' },
];

const EditProfile = ({ profile, values, errors, touched, isSubmitting }) => {
    return (
        <Formik
            initialValues={{
                firstName: profile.firstName || '',
                lastName: profile.lastName || '',
                gender: profile.gender
                    ? {
                          key: 'gender',
                          label: profile.gender,
                          value: profile.gender,
                      }
                    : '',
                dob: profile.dob || '',
                website: profile.website || '',
                bio: profile.bio || '',
                picture: profile.picture || '',
                email: profile.email || '',
            }}
            onSubmit={(values, { setSubmitting, setErrors, resetForm }) => {
                const config = {
                    headers: {
                        'Content-type': 'multipart/form-data',
                        accept: 'application/json',
                        'Accept-Language': 'en-US,en;q=0.8',
                    },
                };

                console.log(values.dob);

                let formData = new FormData();
                formData.append('firstName', values.firstName);
                formData.append('lastName', values.lastName);
                formData.append('dob', values.dob);
                formData.append('website', values.website);
                formData.append('gender', values.gender.value);
                formData.append('bio', values.bio);
                formData.append('email', values.email);
                if (typeof values.picture === 'object') {
                    formData.append(
                        'picture',
                        values.picture.file.originFileObj
                    );
                }
                console.log(values);

                axios
                    .post('/profile', formData, config)
                    .then(response => {
                        console.log(response);
                    })
                    .catch(error => {
                        console.log(error.response);
                    });
            }}
            validationSchema={Yup.object().shape({
                firstName: Yup.string()
                    .min(3, 'First name must be 3 characters or longer')
                    .required('First Name is a required field'),
                lastName: Yup.string()
                    .min(3, 'Last name must be 3 characters or longer')
                    .required('Last Name is a required field'),
                gender: Yup.string().required('Gender is a required field'),
                dob: Yup.string().required('DOB is a required field'),
                bio: Yup.string().max(
                    240,
                    'Bio should be less than 240 characters'
                ),
            })}>
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    setFieldValue,
                } = props;

                const picture =
                    typeof values.picture === 'object'
                        ? URL.createObjectURL(values.picture.file.originFileObj)
                        : values.picture;
                return (
                    <Card className={classes.ProfileCard}>
                        <Card.Content>
                            <Card.Header>Profile</Card.Header>
                            <Card.Meta>Edit your profile</Card.Meta>
                            <div className={classes.ProfileImage}>
                                <Upload
                                    height="100%"
                                    width="100%"
                                    name="picture"
                                    listType="picture-card"
                                    showUploadList={false}
                                    accept="image/*"
                                    onChange={value => {
                                        setFieldValue('picture', value);
                                    }}>
                                    {picture ? (
                                        <img
                                            width="200px"
                                            height="200px"
                                            src={picture}
                                            alt="avatar"
                                        />
                                    ) : (
                                        <div>Upload</div>
                                    )}
                                </Upload>
                            </div>

                            <Form className={classes.Form}>
                                <Grid
                                    className={classes.Grid}
                                    stackable
                                    columns="equal">
                                    <Grid.Column className={classes.Column}>
                                        <Input
                                            label="First name"
                                            name="firstName"
                                            placeholder="First name"
                                            type="text"
                                            errors={errors.firstName}
                                            touched={touched.firstName}
                                        />
                                    </Grid.Column>
                                    <Grid.Column className={classes.Column}>
                                        <Input
                                            label="Last name"
                                            name="lastName"
                                            placeholder="Last name"
                                            type="text"
                                            errors={errors.lastName}
                                            touched={touched.lastName}
                                        />
                                    </Grid.Column>
                                </Grid>

                                <Grid stackable columns="equal">
                                    <Grid.Column className={classes.Column}>
                                        <DatePicker
                                            label="DOB"
                                            name="dob"
                                            placeholder="DD/MM/YY"
                                            type="text"
                                            errors={errors.dob}
                                            touched={touched.dob}
                                            value={values.dob}
                                            onChange={setFieldValue}
                                        />
                                    </Grid.Column>
                                    <Grid.Column className={classes.Column}>
                                        <Select
                                            label="Gender"
                                            name="gender"
                                            placeholder="Gender"
                                            errors={errors.gender}
                                            touched={touched.gender}
                                            onChange={setFieldValue}
                                            value={values.gender}
                                            options={genders}
                                        />
                                    </Grid.Column>
                                </Grid>

                                <Input
                                    label="Website"
                                    name="website"
                                    placeholder="www.xyz.xyz"
                                    type="text"
                                    errors={errors.website}
                                    touched={touched.website}
                                />

                                <TextArea
                                    label="Bio"
                                    name="bio"
                                    placeholder="Say something about yourself"
                                    type="text"
                                    value={values.bio}
                                    onChange={handleChange}
                                    errors={errors.bio}
                                    touched={touched.bio}
                                />

                                <Link to="/profile">
                                    <Button>Cancel</Button>
                                </Link>

                                <Button primary disabled={isSubmitting}>
                                    Save
                                </Button>
                            </Form>
                        </Card.Content>
                    </Card>
                );
            }}
        </Formik>
    );
};

const mapStateToProps = state => {
    return {
        profile: state.profile,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        storeData: userData => dispatch(actionCreators.store(userData)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditProfile);
