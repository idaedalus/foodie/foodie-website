import React, { Component } from 'react';
import ProfileCard from '../../components/ProfileCard/ProfileCard';
import Feed from '../FeedTest/FeedTest';
import classes from './Profile.module.css';
import { connect } from 'react-redux';

class Profile extends Component {
    render() {
        let profile = this.props.profile;

        return (
            <div className={classes.ProfileCard}>
                <ProfileCard
                    userID={profile.userID}
                    name={`${profile.firstName} ${profile.lastName}`}
                    username={profile.username}
                    bio={profile.bio}
                    picture={profile.picture}
                    followers={profile.followers}
                    following={profile.following}
                    totalYumms={profile.yumms}
                />

                <Feed posts={profile.posts} userID={profile.userID} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        profile: state.profile,
    };
};

export default connect(
    mapStateToProps,
    null
)(Profile);
