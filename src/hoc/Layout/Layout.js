import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import { set, get } from '../../util/localStorage';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import Feed from '../../containers/FeedTest/FeedTest';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import CreateNewPost from '../../containers/CreateNewPost/CreateNewPost';
import Profile from '../../containers/Profile/Profile';
import EditProfile from '../../containers/Profile/EditProfile/EditProfile';
import ProfileCard from '../../components/ProfileCard/ProfileCard';
import RecommendedTags from '../../components/Recommendations/Tags/Tags';
import RecommendedPosts from '../../components/Recommendations/Posts/Posts';
import TrendingPosts from '../../components/Recommendations/TrendingPosts/TrendingPosts';
import classes from './Layout.module.css';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import axios from '../../axios/nodeInstance';

class Layout extends Component {
    state = {
        showSideDrawer: false,
        posts: [],
        isNextPageLoading: false,
        page: 0,
    };

    sideDrawerClosedHandler = () => {
        this.setState({ showSideDrawer: false });
    };

    sideDrawerToggleHandler = () => {
        this.setState(prevState => {
            return { showSideDrawer: !prevState.showSideDrawer };
        });
    };

    componentDidMount() {
        axios
            .get('/profile', {
                params: {
                    email: get('email'),
                },
            })
            .then(res => {
                //console.log(res.data);

                let data = res.data;
                this.props.storeData({
                    access_token: data.access_token,
                    username: data.username,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    dob: data.dob,
                    email: data.email,
                    followers: data.followers,
                    following: data.following,
                    yumms: data.likes,
                    token: data.token,
                    bio: data.bio,
                    picture: data.picture,
                    posts: data.posts,
                    gender: data.gender,
                    mobile: data.mobile,
                    userID: data._id,
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        let profile = this.props.profile;

        return (
            <>
                <Toolbar drawerToggleClicked={this.sideDrawerToggleHandler} />
                <SideDrawer
                    closed={this.sideDrawerClosedHandler}
                    open={this.state.showSideDrawer}
                />
                <main className={classes.Content}>
                    <Switch>
                        <Route
                            path="/profile/edit"
                            render={props => <EditProfile />}
                        />
                        <Route path="/profile" component={Profile} />
                        <Route path="/submit" component={CreateNewPost} />
                        <Grid centered>
                            <Grid.Column
                                only="computer"
                                computer={5}
                                className={classes.ProfileColumn}>
                                <ProfileCard
                                    name={`${profile.firstName} ${profile.lastName}`}
                                    userID={profile.userID}
                                    username={profile.username}
                                    bio={profile.bio}
                                    picture={profile.picture}
                                    followers={profile.followers}
                                    following={profile.following}
                                    totalYumms={profile.yumms}
                                />
                                <RecommendedTags />
                            </Grid.Column>

                            <Grid.Column
                                className={classes.ContentColumn}
                                mobile={16}
                                tablet={14}
                                computer={6}>
                                <Route
                                    path="/"
                                    render={props => (
                                        <Feed userID={profile.userID} />
                                    )}
                                />
                            </Grid.Column>

                            <Grid.Column
                                only="computer"
                                computer={5}
                                className={classes.RecommendationColumn}>
                                <TrendingPosts />
                                <RecommendedPosts />
                            </Grid.Column>
                        </Grid>
                    </Switch>
                </main>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        profile: state.profile,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        storeData: userData => dispatch(actionCreators.store(userData)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Layout);
