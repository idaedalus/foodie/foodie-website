import axios from 'axios';
import { get, set } from '../util/localStorage';

let userToken = get('authToken');
let access_token = get('access_token');

const instance = axios.create({
    baseURL: 'https://foodie-application.herokuapp.com/api/v1',
    headers: {
        Authorization: 'Bearer ' + userToken,
        access_token: access_token,
    },
});

export default instance;
