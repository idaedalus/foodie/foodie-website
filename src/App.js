import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import { set, get } from './util/localStorage';
import './App.module.css';
import 'semantic-ui-css/semantic.min.css';
import Auth from './containers/Auth/Auth';
import Layout from './hoc/Layout/Layout';

class App extends Component {
    render() {
        let userToken = get('authToken');

        //let homeRoute = userToken ? <Route path="/" component={Layout} /> : <Redirect to="/auth" />;
        let homeRoute = userToken ? (
            <Route path="/" component={Layout} />
        ) : (
            <Redirect to="/auth" />
        );
        return (
            <div className="App">
                <Switch>
                    <Route path="/auth" component={Auth} />
                    {homeRoute}
                </Switch>
            </div>
        );
    }
}

export default App;
