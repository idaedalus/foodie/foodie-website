import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    dob: '',
    followers: [],
    following: [],
    yumms: 0,
    access_token: '',
    token: '',
    bio: '',
    picture: '',
    gender: '',
    mobile: '',
    posts: [],
    userID: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_PROFILE_DATA:
            return updateObject(state, {
                email: action.userData.email,
                username: action.userData.username,
                access_token: action.userData.access_token,
                token: action.userData.token,
                firstName: action.userData.firstName,
                lastName: action.userData.lastName,
                dob: action.userData.dob,
                followers: action.userData.followers,
                following: action.userData.following,
                yumms: action.userData.yumms,
                bio: action.userData.bio,
                picture: action.userData.picture,
                posts: action.userData.posts,
                gender: action.userData.gender,
                mobile: action.userData.mobile,
                userID: action.userData.userID,
            });

        default:
            return state;
    }
};

export default reducer;
