import * as actionTypes from './actionTypes';

export const store = userData => {
    return {
        type: actionTypes.STORE_PROFILE_DATA,
        userData: userData,
    };
};
