export const set = (key, data) => {
    localStorage.setItem(key, data);
};

export const get = key => {
    return localStorage.getItem(key);
};
