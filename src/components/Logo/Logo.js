import React, { Component } from 'react';
import classes from './Logo.module.css';

import BurgerFilled from '../../assets/burger-filled.svg';
import BurgerOutline from '../../assets/burger-outline.svg';
import LogoImage from './LogoImage/LogoImage';

class Logo extends Component {
    state = {
        logoSrc: BurgerOutline,
    };

    onMouseEnter = () => {
        this.setState({ logoSrc: BurgerFilled });
    };

    onMouseLeave = () => {
        this.setState({ logoSrc: BurgerOutline });
    };

    render() {
        const logoSrc = this.state.logoSrc;

        return (
            <div className={classes.Logo}>
                <LogoImage
                    onMouseEnter={this.onMouseEnter}
                    onMouseLeave={this.onMouseLeave}
                    imageSrc={logoSrc}
                />
                <p
                    onMouseEnter={this.onMouseEnter}
                    onMouseLeave={this.onMouseLeave}>
                    Qripsy
                </p>
            </div>
        );
    }
}

export default Logo;
