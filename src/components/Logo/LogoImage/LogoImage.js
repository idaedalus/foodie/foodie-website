import React from 'react';
import classes from './LogoImage.module.css';

const LogoImage = props => {
    return (
        <img
            className={classes.LogoImage}
            src={props.imageSrc}
            alt="Foodie Logo"
            onMouseEnter={props.onMouseEnter}
            onMouseLeave={props.onMouseLeave}
        />
    );
};

export default LogoImage;
