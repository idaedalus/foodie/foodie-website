import React from 'react';
import classes from './NavigationItems.module.css';
import { Input } from 'semantic-ui-react';
import NavigationItem from './NavigationItem/NavigationItem';

const NavigationItems = props => {
    return (
        <div className={classes.NavigationItems}>
            <Input icon="search" placeholder="Search..." />
            <NavigationItem link="/" exact>
                Home
            </NavigationItem>
            <NavigationItem link="/profile" exact>
                Profile
            </NavigationItem>
            <NavigationItem link="/settings" exact>
                Settings
            </NavigationItem>
        </div>
    );
};

export default NavigationItems;
