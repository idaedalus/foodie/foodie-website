import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './NavigationItem.module.css';

const NavigationItem = props => {
    return (
        <NavLink
            className={classes.NavigationItem}
            activeClassName={classes.active}
            exact={props.exact}
            to={props.link}>
            {props.children}
        </NavLink>
    );
};

export default NavigationItem;
