import React from 'react';
import { Header } from 'semantic-ui-react';

const Tag = props => {
    return (
        <React.Fragment>
            <Header as="h4" >#{props.tagTitle}</Header>
        </React.Fragment>
    );
}

export default Tag;