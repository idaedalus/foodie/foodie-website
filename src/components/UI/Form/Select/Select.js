import React from 'react';
import { Form } from 'semantic-ui-react';
import Select from 'react-select';
import Error from '../Error/Error';
import classes from './Select.module.css';

const Input = props => {
    const handleChange = value => {
        props.onChange(props.name, value);
    };

    return (
        <Form.Field className={classes.Container}>
            <label htmlFor={props.name}>{props.label}:</label>{' '}
            <Select
                name={props.name}
                placeholder={props.placeholder}
                onChange={handleChange}
                value={props.value}
                options={props.options}
            />
            {props.touched && props.errors && <Error errors={props.errors} />}
        </Form.Field>
    );
};

export default Input;
