import React from 'react';
import { DatePicker } from 'antd';
import { Form } from 'semantic-ui-react';
import { Field } from 'formik';
import moment from 'moment';
import Error from '../Error/Error';
import classes from './DatePicker.module.css';

const dateFormat = 'DD/MM/YYYY';

const DatePickerCustom = props => {
    let onChange = (date, dateString) => {
        props.onChange(props.name, dateString);
    };

    return (
        <Form.Field className={classes.Container}>
            <label htmlFor={props.name}>{props.label}:</label>{' '}
            <DatePicker
                name={props.name}
                defaultValue={moment(props.value, dateFormat)}
                format={dateFormat}
                onChange={onChange}
            />
            {props.touched && props.errors && <Error errors={props.errors} />}
        </Form.Field>
    );
};

export default DatePickerCustom;
