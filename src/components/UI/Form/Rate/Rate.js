import React from 'react';
import { Rate, Icon } from 'antd';
import { Form } from 'semantic-ui-react';
import Error from '../Error/Error';
import classes from './Rate.module.css';

const RateComponent = props => {
    const handleChange = value => {
        props.onChange(props.name, value);
    };
    return (
        <Form.Field className={classes.Container}>
            <label htmlFor={props.name}>{props.label}: </label>{' '}
            <Rate
                character={<Icon type={props.icon} theme="filled" />}
                allowHalf
                onChange={handleChange}
                value={props.value}
                name={props.name}
            />
            {props.touched && props.errors && <Error errors={props.errors} />}
        </Form.Field>
    );
};

export default RateComponent;
