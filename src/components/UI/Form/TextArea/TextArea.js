import React from 'react';
import { Form } from 'semantic-ui-react';
import Error from '../Error/Error';
import classes from './TextArea.module.css';

const Input = props => {
    return (
        <Form.Field className={classes.Container}>
            <label htmlFor={props.name}>{props.label}:</label>{' '}
            <textarea
                className={classes.TextArea}
                type={props.type}
                name={props.name}
                placeholder={props.placeholder}
                onChange={props.onChange}
                value={props.value}
            />
            {props.touched && props.errors && <Error errors={props.errors} />}
        </Form.Field>
    );
};

export default Input;
