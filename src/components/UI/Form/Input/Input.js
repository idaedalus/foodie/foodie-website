import React from 'react';
import { Form } from 'semantic-ui-react';
import { Field } from 'formik';
import Error from '../Error/Error';
import classes from './Input.module.css';

const Input = props => {
    return (
        <Form.Field className={classes.Container}>
            <label htmlFor={props.name}>{props.label}:</label>{' '}
            <Form.Input fluid>
                <Field
                    type={props.type}
                    name={props.name}
                    placeholder={props.placeholder}
                />
            </Form.Input>
            {props.touched && props.errors && <Error errors={props.errors} />}
        </Form.Field>
    );
};

export default Input;
