import React from 'react';
import { Form } from 'semantic-ui-react';
import { Switch } from 'antd';
import Error from '../Error/Error';
import classes from './Switch.module.css';

const SwitchComponent = props => {
    const handleChange = value => {
        props.onChange(props.name, value);
    };

    return (
        <Form.Field className={classes.Container}>
            <label htmlFor={props.name}>{props.label}</label>{' '}
            <Switch onChange={handleChange} />
            {props.touched && props.errors && <Error errors={props.errors} />}
        </Form.Field>
    );
};

export default SwitchComponent;
