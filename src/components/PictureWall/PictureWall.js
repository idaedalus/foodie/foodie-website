import React from 'react';
import { Upload, Icon, Modal } from 'antd';

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const actionFunction = file => {
    console.log(file);
    return new Promise(res => setTimeout(() => res(), 1));
};

class PicturesWall extends React.Component {
    state = {
        previewVisible: false,
        previewImage: '',
    };

    handleCancel = () => this.setState({ previewVisible: false });

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    };

    handleChange = ({ fileList }) => {
        this.props.onChange('files', fileList);
    };

    render() {
        const { previewVisible, previewImage } = this.state;
        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        return (
            <div className="clearfix">
                <Upload
                    accept="image/*, video/*"
                    action={actionFunction}
                    listType="picture-card"
                    fileList={this.props.files}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                    beforeUpload={(file, fileList) => {
                        return false;
                    }}>
                    {this.props.files.length > 10 ? null : uploadButton}
                </Upload>
                <Modal
                    visible={previewVisible}
                    footer={null}
                    onCancel={this.handleCancel}>
                    <img
                        alt="example"
                        style={{ width: '100%' }}
                        src={previewImage}
                    />
                </Modal>
            </div>
        );
    }
}

export default PicturesWall;
