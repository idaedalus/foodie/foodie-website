import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';

const EditProfileButton = props => {
    return (
        <Link to="/profile/edit">
            <Button fluid basic color="blue">
                Edit Profile
            </Button>
        </Link>
    );
};

export default EditProfileButton;
