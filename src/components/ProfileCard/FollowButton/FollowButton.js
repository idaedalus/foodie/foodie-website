import React from 'react';
import { Button } from 'semantic-ui-react';

const FollowButton = props => {
    const button = props.follow ? (
        <Button fluid color="blue">
            Unfollow
        </Button>
    ) : (
        <Button fluid basic color="blue">
            Follow
        </Button>
    );
    return <React.Fragment>{button}</React.Fragment>;
};

export default FollowButton;
