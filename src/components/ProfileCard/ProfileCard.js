import React from 'react';
import { Image, Header, Icon, Card, Grid, Button } from 'semantic-ui-react';
import classes from './ProfileCard.module.css';
import EditProfileButton from './EditProfileButton/EditProfileButton';
import FollowButton from './FollowButton/FollowButton';
import Burger from '../../assets/burger-filled.svg';
import Group from '../../assets/group.svg';
import User from '../../assets/user.svg';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

const ProfileCard = props => {
    let follow = false;
    if (props.userID in props.profile.followers) {
        follow = true;
    }

    let button =
        props.userID === props.profile.userID ? (
            <EditProfileButton />
        ) : (
            <FollowButton follow={follow} />
        );

    return (
        <>
            <Card className={classes.ProfileCard}>
                <Card.Content>
                    <Grid padded textAlign="center">
                        <Grid.Row>
                            <Grid.Column width={16} textAlign="center">
                                <Image
                                    circular
                                    src={props.picture}
                                    size="tiny"
                                />
                                <Header as="h3">
                                    <b>{props.name}</b>
                                </Header>
                                <Header
                                    as="h4"
                                    style={{ margin: '0px', padding: '0px' }}>
                                    {props.username}
                                    {/* <Icon name="at" /> */}
                                </Header>
                                <p>{props.bio ? props.bio : null}</p>
                                {button}
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Card.Content>
            </Card>

            <Card className={classes.ProfileCard}>
                <Card.Content>
                    <Grid
                        centered
                        columns={3}
                        width="equal"
                        padded
                        textAlign="center"
                        verticalAlign="middle">
                        <Grid.Column textAlign="center">
                            {/*  <Image className={classes.Icon} src={Group} /> */}
                            <Header as="h4">
                                <b>
                                    {props.followers
                                        ? props.followers.length
                                        : 0}
                                </b>
                            </Header>
                            Followers
                        </Grid.Column>
                        <Grid.Column textAlign="center">
                            {/* <Image className={classes.Icon} src={User} /> */}
                            <Header as="h4">
                                <b>
                                    {props.following
                                        ? props.following.length
                                        : 0}
                                </b>
                            </Header>
                            Following
                        </Grid.Column>
                        <Grid.Column textAlign="center">
                            {/* <Image className={classes.Icon} src={Burger} /> */}
                            <Header as="h4">
                                <b>{props.totalYumms ? props.totalYumms : 0}</b>
                            </Header>
                            Yumms
                        </Grid.Column>
                    </Grid>
                </Card.Content>
            </Card>
        </>
    );
};

const mapStateToProps = state => {
    return {
        profile: state.profile,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        storeData: userData => dispatch(actionCreators.store(userData)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileCard);
