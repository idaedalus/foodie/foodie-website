import React from 'react';
import { Card } from 'semantic-ui-react';
import classes from './Posts.module.css';

const Posts = props => {
    return (
        <Card className={classes.PostsCard}>
            <Card.Content>
                <Card.Header>Recommended Posts</Card.Header>
            </Card.Content>
        </Card>
    );
}

export default Posts;