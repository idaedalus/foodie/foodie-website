import React from 'react';
import { Card, Feed, Image } from 'semantic-ui-react';
import classes from './TrendingPosts.module.css';

//const image = 'https://react.semantic-ui.com/images/avatar/small/laura.jpg'
const image = "https://react.semantic-ui.com/images/avatar/large/matthew.png"
const username = 'burhanuday'
const summary = 'Recipe on punjabi tadka'

const TrendingPosts = props => {
    return (
        <Card className={classes.TrendingPostsCard}>
            <Card.Content>
                <Card.Header>Trending Posts</Card.Header>
            </Card.Content>
            <Card.Content>
                <Feed>
                    <Feed.Event>
                        <Feed.Label image={image} />
                        <Feed.Content>
                            <Feed.Date>{username} • few hours ago</Feed.Date>
                            <Feed.Summary>{summary}</Feed.Summary>
                            <Feed.Extra images>
                            <Image src={image} />
                                <Image src={image} />
                            </Feed.Extra>
                        </Feed.Content>
                    </Feed.Event>
                </Feed>

                <Feed>
                    <Feed.Event>
                        <Feed.Label image={image} />
                        <Feed.Content>
                            <Feed.Date>{username} • few hours ago</Feed.Date>
                            <Feed.Summary>{summary}</Feed.Summary>
                            <Feed.Extra images>
                            <Image src={image} />
                                <Image src={image} />
                            </Feed.Extra>
                        </Feed.Content>
                    </Feed.Event>
                </Feed>

            </Card.Content>
        </Card>
    );
}

export default TrendingPosts;