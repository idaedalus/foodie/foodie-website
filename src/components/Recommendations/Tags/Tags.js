import React from 'react';
import { Card } from 'semantic-ui-react';
import classes from './Tags.module.css';

import Tag from '../../UI/Tag/Tag';

const Tags = props => {
    return (
        <Card className={classes.TagsCard}>
            <Card.Content>
                <Card.Header>Recommended Tags</Card.Header>
                <Card.Description>
                    <Tag tagTitle="Hello" />
                </Card.Description>

            </Card.Content>
        </Card>
    );
}

export default Tags;