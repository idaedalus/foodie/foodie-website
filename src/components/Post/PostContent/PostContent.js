import React from 'react';
import { Collapse } from 'react-bootstrap';
import { Card } from 'semantic-ui-react';
import Restaurant from './Restaurant/Restaurant';
import Recipe from './Recipe/Recipe';

const PostContent = props => {
    let content = props.restaurant ? (
        <Restaurant restaurant={props.restaurant} />
    ) : (
        <Recipe recipe={props.recipe} />
    );
    return (
        <Card.Content>
            <Card.Header>{props.title} </Card.Header>
            <Collapse in={props.postExpanded}>
                <div>
                    <Card.Description>{props.description}</Card.Description>
                    <div>{content}</div>
                </div>
            </Collapse>
        </Card.Content>
    );
};

export default PostContent;
