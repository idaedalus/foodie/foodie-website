import React from 'react';
import IconArray from '../../IconArray/IconArray';

const Recipe = props => {
    return (
        <>
            <IconArray
                alcohol={props.recipe.isBar}
                parking={props.recipe.isValet}
            />

            <p>Cooking time: {props.recipe.cooking_time}</p>
            <p>Difficulty: {props.recipe.difficulty}</p>
            <p>Ingredients: {props.recipe.ingredients}</p>
            <p>Type of food: {props.recipe.typeOfFood}</p>
            <p>Type of recipe: {props.recipe.typeOfRecipe}</p>
        </>
    );
};

export default Recipe;
