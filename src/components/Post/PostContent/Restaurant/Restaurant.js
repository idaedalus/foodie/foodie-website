import React from 'react';
import IconArray from '../../IconArray/IconArray';
import { Rating } from 'semantic-ui-react';

const Restaurant = props => {
    let restaurant = props.restaurant;
    return (
        <>
            <IconArray
                alcohol={props.restaurant.isBar}
                parking={props.restaurant.isValet}
            />
            <p>Restaurant: {restaurant.restaurant_name}</p>
            <p>Location: {restaurant.location}</p>
            <p>Place: {restaurant.place}</p>
            <p>Price: {restaurant.price}</p>
            Rating:
        </>
    );
};

export default Restaurant;
