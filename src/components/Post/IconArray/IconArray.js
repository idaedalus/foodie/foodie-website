import React from 'react';
import { Image, Popup } from 'semantic-ui-react';

import Leaf from '../../../assets/leaf.svg';
import Parking from '../../../assets/parking.svg';
import Beer from '../../../assets/beer.svg';
import Chicken from '../../../assets/chicken-leg.svg';

import classes from './IconArray.module.css';

const IconArray = props => {
    let veg = props.veg ? (
        <Popup
            content="Vegetarian food"
            trigger={<Image className={classes.Icon} src={Leaf} />}
        />
    ) : null;

    let parking = props.parking ? (
        <Popup
            content="Valet parking available"
            trigger={<Image className={classes.Icon} src={Parking} />}
        />
    ) : null;

    let alcohol = props.alcohol ? (
        <Popup
            content="Alcohol available"
            trigger={<Image className={classes.Icon} src={Beer} />}
        />
    ) : null;

    let nonVeg = props.nonVeg ? (
        <Popup
            content="Non veg food"
            trigger={<Image className={classes.Icon} src={Chicken} />}
        />
    ) : null;

    return (
        <div className={classes.IconArray}>
            {veg}
            {parking}
            {alcohol}
            {nonVeg}
        </div>
    );
};

export default IconArray;
