import React from 'react';
import classes from './Yumm.module.css';

const Yumm = props => {
    return (
        <img
            className={classes.Yumm}
            src={props.imageSrc}
            alt="Yumm this"
            onClick={props.clicked}
        />
    );
};

export default Yumm;
