import React from 'react';
import Slider from 'react-slick';

const Carousel = props => {
    let settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: false,
        arrows: true,
    };

    return <Slider {...settings}>{props.children}</Slider>;
};

export default Carousel;
