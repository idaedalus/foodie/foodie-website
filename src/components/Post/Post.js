import React, { useState, useEffect } from 'react';
import { Card, Image } from 'semantic-ui-react';
import PostContent from './PostContent/PostContent';
import classes from './Post.module.css';
import ArrowButton from './ArrowButton/ArrowButton';
import Yumm from './Yumm/Yumm';
import Carousel from './Carousel/Carousel';
import BurgerFilled from '../../assets/burger-filled.svg';
import BurgerOutline from '../../assets/burger-outline.svg';
import axios from '../../axios/nodeInstance';
import moment from 'moment';

const Post = props => {
    const [postYummed, setPostYummed] = useState(false);
    const [postExpanded, setPostExpanded] = useState(false);
    const [useResponseLikes, setUseResponseLikes] = useState(false);
    const [likes, setLikes] = useState(0);

    useEffect(() => {
        if (props.liked_by.includes(props.userID)) {
            setPostYummed(true);
        }
    }, []);

    const yummIconHandler = () => {
        axios
            .get('/like', {
                params: {
                    userID: props.userID,
                    postId: props.postID,
                },
            })
            .then(res => {
                console.log(res.data);

                if (res.status === 200) {
                    setPostYummed(!postYummed);
                    setLikes(res.data.data.likes);
                    setUseResponseLikes(true);
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    const expandPostHandler = () => {
        setPostExpanded(!postExpanded);
    };

    const yummIcon = postYummed ? BurgerFilled : BurgerOutline;
    let ImageArray = props.imageUrl.map(imageUrl => {
        return <Image key={imageUrl} src={imageUrl} />;
    });

    let likesCounter = useResponseLikes ? likes : props.likes;

    let date = moment(props.date).format('HH:mm DD MMM');

    return (
        <div className={classes.Post}>
            <Card className={classes.Card}>
                <Card.Content>
                    <div style={{ display: 'flex' }}>
                        <Image
                            src={props.picture}
                            avatar
                            style={{ display: 'inline' }}
                        />
                        <h3 style={{ display: 'inline', margin: 'auto 0px' }}>
                            <b>{props.username}</b>
                        </h3>
                    </div>

                    <Card.Meta>
                        <span className="date">{date}</span>
                    </Card.Meta>
                </Card.Content>
                <Carousel>{ImageArray}</Carousel>
                <Card.Content extra>
                    <Yumm imageSrc={yummIcon} clicked={yummIconHandler} />
                    {likesCounter} Yumms
                </Card.Content>

                <PostContent
                    title={props.title}
                    description={props.description}
                    restaurant={props.restaurant}
                    recipe={props.recipe}
                    postExpanded={postExpanded}
                />
            </Card>
            <div className={classes.ArrowButton}>
                <ArrowButton
                    direction={postExpanded}
                    clicked={() => expandPostHandler()}
                />
            </div>
            {/* <Comments postExpanded={postExpanded} /> */}
        </div>
    );
};

export default Post;
