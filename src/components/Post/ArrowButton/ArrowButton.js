import React from 'react';
import { Icon } from 'semantic-ui-react';
import classes from './ArrowButton.module.css';

const ArrowButton = props => {

    let arrowIcon = props.direction? <Icon className={classes.ArrowIcon} name='angle up' size='huge' /> : <Icon className={classes.ArrowIcon} name='angle down' size='huge' />
    return (
        <div onClick={() => props.clicked()} className={classes.ArrowButton} >
            {arrowIcon}
        </div>

    );
}

export default ArrowButton;