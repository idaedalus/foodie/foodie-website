import React from 'react';
import { Card, Comment, Header, Form, Button } from 'semantic-ui-react';
import { Collapse } from 'react-bootstrap';
import classes from '../Post.module.css';

const Comments = props => {
    return (
        <Collapse in={props.postExpanded}>
            <div>
                <Card className={classes.Card}>
                    <Card.Content>
                        <Header as="h4">Comments</Header>
                        <Comment.Group>
                            <Comment>
                                <Comment.Avatar src="https://react.semantic-ui.com/images/avatar/large/matthew.png" />
                                <Comment.Content>
                                    <Comment.Author as="a">Matt</Comment.Author>
                                    <Comment.Metadata>
                                        <div>Today at 5:42PM</div>
                                    </Comment.Metadata>
                                    <Comment.Text>How artistic!</Comment.Text>
                                </Comment.Content>
                            </Comment>

                            <Comment>
                                <Comment.Avatar src="https://react.semantic-ui.com/images/avatar/small/elliot.jpg" />
                                <Comment.Content>
                                    <Comment.Author as="a">
                                        Elliot Fu
                                    </Comment.Author>
                                    <Comment.Metadata>
                                        <div>Yesterday at 12:30AM</div>
                                    </Comment.Metadata>
                                    <Comment.Text>
                                        <p>
                                            This has been very useful for my
                                            research. Thanks as well!
                                        </p>
                                    </Comment.Text>
                                </Comment.Content>
                            </Comment>

                            <Form reply>
                                <Form.TextArea
                                    rows={2}
                                    placeholder="Reply to this post"
                                    style={{ height: '50px' }}
                                />
                                <Button
                                    content="Add Reply"
                                    labelPosition="left"
                                    icon="edit"
                                    primary
                                />
                            </Form>
                        </Comment.Group>
                    </Card.Content>
                </Card>
            </div>
        </Collapse>
    );
};

export default Comments;
